/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
const { app, BrowserWindow, ipcMain, dialog } = require('electron');
const fs = require('fs');
const path = require('path');
const version = require('./openblock.json');
const https = require('https');
const util = require('util');


// if (require('electron-squirrel-startup')) return app.quit();

app.commandLine.appendSwitch("--disable-http-cache");
app.commandLine.appendSwitch('trace-warnings')
process.traceProcessWarnings = true;
const getPort = require('get-port');
const server = require('./server');
const discover = require('./discover');
let serverPort = -1;
console.log(app.getAppPath());
if (app.commandLine.hasSwitch('port')) {
    let str_port = app.commandLine.getSwitchValue('port');
    let port = Number.parseInt(str_port);
    if (Number.isInteger(port)) {
        if (port > 1 && port <= 65535) {
            serverPort = port;
        }
    }
}
let MAIN_WINDOW_NAME_PROFIX = "OpenBlock IDE: ";
let mainWindow;
function createWindow() {
    if (serverPort <= 0) {
        throw Error('Server has not started.');
    }
    let preload = path.join(app.getAppPath(), '/preload.js');
    console.log('preload:' + preload);
    mainWindow = new BrowserWindow({
        width: 1180,
        height: 800,
        webPreferences: {
            preload,
            devTools: true,
            contextIsolation: false
        },
        title: MAIN_WINDOW_NAME_PROFIX + serverPort
    });
    // mainWindow.webContents.openDevTools();
    setTimeout(() => {
        let url = `http://localhost:${serverPort}/index.html`;
        console.log(url);
        mainWindow.loadURL(url);
        // mainWindow.title = MAIN_WINDOW_NAME_PROFIX;
    }, 1000);
}


app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow();
    }
});


let staticpath = "./openblock.zip";
if (!fs.existsSync(staticpath)) {
    console.log("can't find " + path.resolve(staticpath));
    staticpath = './resources/app/openblock.zip';
}
if (!fs.existsSync(staticpath)) {
    console.log("can't find " + path.resolve(staticpath));
    app.exit();
    return;
}
console.log(staticpath);
let jsonpath = path.join(path.dirname(staticpath), 'openblock.json');

async function download(url) {
    return new Promise((resolve, rej) => {
        https.get(url, (res) => {
            let buf_arr = [];
            res.on('data', (d) => {
                buf_arr.push(d);
            });
            res.on('end', () => {
                let buf = Buffer.concat(buf_arr);
                resolve(buf);
            });
            res.on('error', (e) => {
                rej(e);
            });
        });
    });
}
app.whenReady().then(() => {
    if (serverPort < 1) {
        getPort().then((p) => {
            serverPort = p;
            server.start(p, staticpath, () => {
                discover.start(p);
                createWindow();
            });
        });
    } else {
        server.start(serverPort, staticpath, () => {
            discover.start(serverPort);
            createWindow();
        });
    }
    download(version.checkUrl).then((data) => {
        let decoder = new util.TextDecoder();
        let newVersion = JSON.parse(decoder.decode(data));
        console.log(version, newVersion);
        if (version.version != newVersion.version) {
            download(newVersion.resourceUrl).then((data) => {
                fs.writeFileSync(staticpath, new Uint8Array(data));
                // .then(() => {
                fs.writeFileSync(jsonpath, JSON.stringify(newVersion));
                // }).then(() => {
                dialog.showMessageBox({
                    message: '已经下载最新版，重启以完成更新。',
                    type: 'question',
                    buttons: ['重启', '取消'],
                    defaultId: 0,
                    title: '更新'
                }).then(r => {
                    console.log(r);
                    if (r.response == 0) {
                        app.relaunch();
                        app.quit();
                    }
                });
            }).catch(e => {
                console.error(e);
            });
        }
    }
    ).catch((e) => {
        console.error(e);
    });
});

app.on('window-all-closed', () => {
    // if (process.platform !== 'darwin') {
    app.quit();
    // }
});
// ipcMain.on('setProjectInfo', (event, info) => {
//     let path = info.path;
//     let name = info.name;
//     discover.setMessage(name);
//     server.setProjectPath(path)
// });
ipcMain.handle('dialog', async (event, methdoName, ...args) => {
    const result = dialog[methdoName](mainWindow, ...args);
    return result
});
let serverCmdHandler = {
    setProjectInfo(info) {
        if (info) {
            discover.setMessage(info.title);
            mainWindow.setTitle(info.title + "-" + MAIN_WINDOW_NAME_PROFIX + serverPort);
        } else {
            mainWindow.setTitle(MAIN_WINDOW_NAME_PROFIX + serverPort);
        }
        server.setProjectPath(info)
    }
};
ipcMain.handle('server', (evt, methodName, ...args) => {
    return serverCmdHandler[methodName](...args);
});
ipcMain.on("appPath", (evt) => {
    evt.returnValue = app.getAppPath();
});
ipcMain.on("userData", (evt) => {
    evt.returnValue = app.getPath('userData');
});