# Electron 编辑器封装

使用electron包装OpenBlock编辑器，包含自动更新的能力。

每次启动时判断联网，并自动下载最新版OpenBlock编辑器。

编辑器启动后，可以对外提供http服务。

支持从命令行启动时通过参数指定服务端口。

例如：
> OpenBlockIDE.exe . --port=80